const gulp = require('gulp'),
  sass = require('gulp-sass'),
  concat = require('gulp-concat'),
  babel = require('gulp-babel'),
  autoprefixer = require('gulp-autoprefixer'),
  clean = require('gulp-clean'),
  minifyjs = require('gulp-js-minify'),
  browserSync = require('browser-sync');


const path = {
  dist: {
    html: 'dist',
    css: 'dist/css',
    js: 'dist/js',
    img: 'dist/img/',
    self: 'dist'
  },
  src: {
    html: 'src/*.html',
    scss: 'src/scss/**/*.scss',
    js: 'src/js/*.js',
    img: 'src/img/**/*'
  }
};

/***********************FUNCTIONS***********************/

const htmlBuild = () => (
  gulp.src(path.src.html)
    .pipe(gulp.dest(path.dist.html))
    .pipe(browserSync.stream())
);
const imgBuild = () => (
  gulp.src(path.src.img)
    .pipe(gulp.dest(path.dist.img))
    .pipe(browserSync.stream())
);
const scssBuild = () => (
  gulp.src(path.src.scss)
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer(['> 0.01%', 'last 100 versions']))
    .pipe(gulp.dest(path.dist.css))
    .pipe(browserSync.stream())
);
const jsBuild = () => (
  gulp.src(path.src.js)
    .pipe(concat('script.js'))
    .pipe(gulp.dest(path.dist.js))
    .pipe(babel({
      presets: ['@babel/env']
    }))
    .pipe(browserSync.stream())
);

const jsMin = () => (
  gulp.src(path.src.js)
    .pipe(minifyjs())
    .pipe(gulp.dest(path.dist.js)
    ));

const cleanDist = () => (
  gulp.src(path.dist.self, {allowEmpty: true})
    .pipe(clean())
    .pipe(browserSync.stream())
);

/************************WATCHERS***********************/

const watcher = () => {
  browserSync.init({
    server: {
      baseDir: "./dist"
    }
  });
  gulp.watch(path.src.html, htmlBuild).on('change', browserSync.reload);
  gulp.watch(path.src.scss, scssBuild).on('change', browserSync.reload);
  gulp.watch(path.src.js, jsBuild).on('change', browserSync.reload);
  gulp.watch(path.src.img, imgBuild).on('change', browserSync.reload);
};

/************************TASKS***********************/

gulp.task('html', htmlBuild);
gulp.task('scss', scssBuild);
gulp.task('img', imgBuild);
gulp.task('default', gulp.series(
  cleanDist,
  htmlBuild,
  scssBuild,
  // jsBuild,
  jsMin,
  imgBuild,
  watcher
));
gulp.task('minify-js', jsMin);