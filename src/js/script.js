$(document).ready(function(){
  // Activate Carousel
  $("#topCarousel").carousel();



  // Enable Carousel Controls
  $(".carousel-control-prev").click(function(){
    $("#topCarousel").carousel("prev");
  });
  $(".carousel-control-next").click(function(){
    $("#topCarousel").carousel("next");
  });
});



  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
  });

// PRODUCTS LOAD MORE

$(function () {
  $(".furniture-gallery .card").slice(0, 3).show();
  $("#productsLoadMore").on('click', function (e) {
    e.preventDefault();
    $("div.furniture-gallery .card:hidden").slice(0, 3).slideDown();
    if ($("div.furniture-gallery .card:hidden").length == 0) {
      $("#productsLoadMore").fadeOut('slow');
    }
    $('div.furniture-gallery').animate({
      scrollTop: $(this).offset().top
    }, 1500);
  });
});
